# Madeleine Gabriela Urresta Jumbo
<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/io.jpg " width ="125">

Mi nombre es Madeleine Gabriela Urresta Jumbo, nací el 30 de mayo del año 2000 en la ciudad de Quito en Ecuador, mis padres se llaman Xavier Marcelo Urresta Rodriguez y María Gabriela Jumbo Dueñas. Viví gran parte de mi vida desde que nací hasta los 10 años en el barrio La Colmena en el centro de Quito, luego viví en la Floresta hasta mis 15 años donde volví a La Colmena para luego y a mis 19 años irme a vivir a Sangolquí. 

Mis pasatiempos son escuchar musica, estudiar, dormir y estar con mi madre.

Mis primeros años de vida estudié en el la escuela “Santa Dorotea”, en mis años de adolescencia estudié en el colegio “Francisca de las Llagas” y actualmente en mis años de adultez estoy cursando la tecnología de Desarrollo de Software en el instituto “Benito Juárez”, aspiro a ser una muy buena desarrolladora en varios campos.