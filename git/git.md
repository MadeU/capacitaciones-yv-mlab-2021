
# Menú
- [Qué es git?](#Qué-es-git?)
- [Comandos de git en consola](#Comandos-de-git-en-consola)
- [Clientes git](#Clientes-git)
- [Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)
- [Merge](#Merge)

#  Qué es git?

* Git es un sistema de control de versiones distribuido de código abierto y gratuito diseñado para manejar todo, desde proyectos pequeños a muy grandes, con velocidad y eficiencia. 

Git es fácil de aprender y ocupa poco espacio con un rendimiento increíblemente rápido . Supera a las herramientas SCM como Subversion, CVS, Perforce y ClearCase con características como bifurcaciones locales económicas , áreas de preparación convenientes y múltiples flujos de trabajo. 

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-16%20(10).png" width ="500">

# Comandos de git en consola

- git init

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-16%20(3).png " width ="400">

- git status

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(1).png " width ="400">

- git touch

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(1).png" width ="400">

- git add .

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(2).png" width ="400">

- git commit -m""

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(3).png" width ="400">

- git push

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(4).png" width ="400">

# Clientes git

* Un cliente GIT o software de control de versiones se usa mayormente para gestionar código fuente. Se diseñó para el mantenimiento de las versione de las aplicaciones cuando tienen un código fuente que contiene muchos archivos.

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(5).png" width ="400">


# Clonación de proyectos por consola y por cliente
*Abrimos el git bash y colocamos git clone y pegamos la direccion url de nuestro proyecto en git lab.

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(8).png" width ="400">

*Abrimos kraken y escogemos la opcion clone y escogemos por donde vamos a clonar en mi caso es git lab y finalmente seleccionamos la carpeta en donde se vaa clonar.

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(7).png" width ="400">

# Commits por consola y por cliente kraken o smart

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(5).png" width ="400">

# Ramas de kraken

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(11).png" width ="400">

# Marge

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(12).png" width ="400">
