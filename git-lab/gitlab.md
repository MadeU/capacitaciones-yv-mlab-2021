# Menú
- [Información de como acceder](#Información-de-como-acceder)
- [Crear repositorios](#Crear-repositorios)
- [Crear grupos](#Crear-grupos)
- [Crear subgrupos](#Crear-subgrupos)
- [Crear issues](#Crear-issues)
- [Crear labels](#Crear-labels)
- [Roles que cumplen](#Roles-que-cumplen)
- [Agregar miembros](#Agregar-miembros)
- [Crear borads y manejo de boards](#Crear-borads-y-manejo-de-boards)

# Información de como acceder

 1. Vamos a nuestro navegador de preferencia y buscamos gitlab.  https://gitlab.com/

 <img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(14).png" width ="400">

 2. nos ponemos en registrabos y nos registramos o solo ingresamos a nuestra cuenta.

 <img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(15).png " width ="400">

# Crear repositorios

1. Dentro de nuestro git lab y seleccionamos la opción  proyectos.
2. Creamos un proyecto en clanco y le ponemos un nombre cualquiera a nuestro proyecto.
3. Vamos a repositorios y ya debiamos haber subido nuestros docmuentos con la ayuda de kraken o desde consola.

# Crear grupos

* Por ejemplo, puede crear un grupo para los miembros de su empresa y un subgrupo para cada equipo individual. Puede asignar un nombre al grupo company-team, y los subgrupos backend-team, frontend-teamy production-team.

Entonces tú puedes:

1. Otorgue a los miembros acceso a varios proyectos a la vez.
2. Agregue tareas pendientes para todos los miembros del grupo a la vez.
3. Vea los problemas y fusione solicitudes de todos los proyectos del grupo, juntos en una sola vista de lista.
4. Edición masiva de problemas, épicas y solicitudes de combinación.
También puede crear subgrupos 

# Crear issues

* Apenas tengamos creado el repositorio, nos vamos a nuestra consola de preferencia y realizamos la subida del proyecto angular que hemos creado con anterioridad, tenemos que ejecutar estos comandos git para empezar.

1. Luego de esto, nos vamos al gitlab en el sidebar lateral izquierdo seleccionamos la opción Issue -> List y presionamos New Issue.
2. Luego ingresamos los siguientes datos:
    * Titulo
    * Descripción
    * Assigne
    * Milestone
    * Labels

# Crear labels

 1. Acceder al repositorio

2. Acceder a Repositorio -> Tags  o Etiquetas

3. Crear una nueva Etiqueta con los datos completos que nos interesen

4. Al guardar, todos los campos completos, en unos minutos vamos a ver dentro de Proyecto -> Versiones la versión para descargar en diferentes formatos y la posibilidad de descargar el código fuente del repositorio al momento de generar la etiqueta.

# Roles que cumplen 

<img src="https://gitlab.com/MadeU/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/2021-03-17%20(17).png" width ="400">


# Agregar miembros

1.  inicie sesion en su cuenta de GitLab y navegue hasta su proyecto en Proyectos.
2. Luego haga clic en la opcion Miembros debajo de la pestana Configuracion.
3. Esto abrira la pantalla de abajo para agregar el miembro a su proyecto.
4. Ahora ingrese el nombre d 'usuario, permiso de funcion, fecha de vencimiento (opcional) y haga clic en el boton Agregar al proyecto para agregar un usuario al proyecto.
5. luego recibira un mensaje exitoso despues de agregar el usuario al proyecto.
6. Tambien puede agregar un usuario al proyecto haciendo clic en el boton Importar.
7. Ahora seleccione el proyecto desde el cual desea agregar el usuario para su proyecto y haga clic en el boton Importar miembros del proyecto.
8. Recibira su mensaje de exito despues de importar el usuario al proyecto.

# Crear boards y manejo de boards

1. Seleccionamos Cuestiones.
2. vamos a tableros.
3. ahi nos aparecera una opcion que dice crear tablero.
4. le damos en el simbolo de mas y creamos cada uno de nuestros proyectos que realizaremos.

5. Para su manejo seleccionamos encima del nombre de cada proyecto y se nos abre una ventana para configurarlo.